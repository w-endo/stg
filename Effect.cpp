#include "Effect.h"
#include "Scene.h"

//コンストラクタ
Effect::Effect()
{
	_frame = 0;
}

//デストラクタ
Effect::~Effect()
{
}

//更新処理
void Effect::Update()
{
	//表示する範囲を求める
	_srcX += _frame * _width;

	//画面外になったらエフェクト消去
	if (_srcX > _size.x)
	{
		_scene->Destroy(this);
	}

	//まだなら次のフレームへ
	else
	{
		_frame++;
	}
}