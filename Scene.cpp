#include "Game.h"
#include "Scene.h"
#include "GameObject.h"

//コンストラクタ
Scene::Scene()
{
}

//デストラクタ
Scene::~Scene()
{
	//そのシーンの全オブジェクトを開放
	for (int i = 0; i < _gameObject.size(); i++)
	{
		delete _gameObject[i];
	}
}

//更新処理
void Scene::Update()
{
	//そのシーンの全オブジェクトを更新
	for (int i = 0; i < _gameObject.size(); i++)
	{
		_gameObject[i]->Update();

		//次のシーンが決まっているなら、このフレームは強制終了
		if (Game::IsNextScene())
		{
			return;
		}
	}

	//そのシーンの全オブジェクトを描画
	for (int i = 0; i < _gameObject.size(); i++)
	{
		_gameObject[i]->Draw();
	}
}

//オブジェクトをシーンに追加
void Scene::AddObject(GameObject* object)
{
	//配列に追加
	_gameObject.push_back(object);

	//どのシーンに追加されたのかを伝える
	object->SetScene(this);
}

//指定したゲームオブジェクトをシーンから削除する
void Scene::Destroy(GameObject* object)
{
	//削除するオブジェクトを配列から探す
	for (int i = 0; i < _gameObject.size(); i++)
	{
		//見つけた
		if (_gameObject[i] == object)
		{
			//メモリ開放
			delete _gameObject[i];

			//配列から消去
			_gameObject.erase(_gameObject.begin() + i);
			
			break;
		}
	}
}

