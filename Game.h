#pragma once

class Scene;

/**************************************
　ゲーム全体を管理するクラス
 ***************************************/
class Game
{
	//クラス変数
	static Scene* _scene;		//現在のシーン
	static Scene* _nextScene;	//次のシーン

	
public:
	//デストラクタ
	~Game();

	//初期化処理
	//引数：なし
	//戻値：なし
	void Start();

	//更新処理
	//引数：なし
	//戻値：なし
	void Update();

	//シーン切り替え（予約）
	//引数：次のシーンのオブジェクト
	//戻値：なし
	static void ChangeScene(Scene* scene);

	//次のシーンが決まっているか
	//引数：なし
	//戻値：決まっている→true　決まってない→false
	static bool IsNextScene();

};

