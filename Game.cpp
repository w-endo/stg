#include "Game.h"
#include "PlayScene.h"

//クラス変数初期化
Scene* Game::_scene = NULL; 
Scene* Game::_nextScene = NULL;


//デストラクタ
Game::~Game()
{
	delete _scene;
}

//初期化処理
void Game::Start()
{
	_scene = new PlayScene;
	_scene->Start();
}


//更新処理
void Game::Update()
{
	//各ゲームオブジェクトの更新と描画
	_scene->Update();


	//次のシーンが決まっていたら切り替え
	if (_nextScene)
	{
		delete _scene;			//現在のシーンを開放
		_scene = _nextScene;	//シーンを切り替え
		_scene->Start();		//新しいシーンの初期化
		_nextScene = NULL;		//次のシーンは未定
	}
}


//シーン切り替え予約
void Game::ChangeScene(Scene* scene)
{
	//次のシーン決定
	_nextScene = scene;
}


//次のシーンが決まっているか
bool Game::IsNextScene()
{
	return _nextScene != NULL;
}
