#pragma once
#include "Scene.h"

/**************************************
　クリアシーンクラス
 ***************************************/
class ClearScene :	public Scene
{
public:
	//コンストラクタ
	ClearScene();

	//デストラクタ
	~ClearScene();

	//初期化処理
	//引数：なし
	//戻値：なし
	void Start() override;
};

