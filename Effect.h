#pragma once
#include "Sprite.h"

/**************************************
　エフェクトクラス
 ***************************************/
class Effect : public Sprite
{
	int _frame;	//現在表示しているフレーム


public:
	//コンストラクタ
	Effect();

	//デストラクタ
	~Effect();

	//更新処理
	//引数：なし
	//戻値：なし
	void Update() override;
};

