#pragma once
#include "Sprite.h"

/**************************************
　敵クラス
 ***************************************/
class Enemy : public Sprite
{
	const int MAX_SPEED;	//最高速度
	VECTOR _move;			//移動値

public:
	//コンストラクタ
	Enemy();

	//デストラクタ
	~Enemy();

	//更新処理
	//引数：なし
	//戻値：なし
	void Update() override;
};

