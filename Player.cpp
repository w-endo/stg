#include "Player.h"
#include "BulletManager.h"
#include "Label.h"
#include "Game.h"
#include "ClearScene.h"


//コンストラクタ
Player::Player() :SPEED(5)
{
	//初期位置
	_position.x = WINDOW_WIDTH / 2;
	_position.y = WINDOW_HEIGHT * 2 / 3;

	//パラメータ初期化
	_score = 0;
	_life = 3;
	_isShot = false;

	//画像の読み込み
	Load("Assets\\player.png");
}

//デストラクタ
Player::~Player()
{
}


//更新処理
void Player::Update()
{
	//移動処理
	Move();

	//弾を撃つ
	Shot();
}

//移動
void Player::Move()
{
	if (CheckHitKey(KEY_INPUT_LEFT))
	{
		_position.x -= SPEED;
	}

	if (CheckHitKey(KEY_INPUT_RIGHT))
	{
		_position.x += SPEED;
	}

	if (CheckHitKey(KEY_INPUT_UP))
	{
		_position.y -= SPEED;
	}

	if (CheckHitKey(KEY_INPUT_DOWN))
	{
		_position.y += SPEED;
	}
}


//弾を撃つ
void Player::Shot()
{
	//攻撃ボタンが押された
	if (CheckHitKey(KEY_INPUT_SPACE))
	{
		//まだ撃ってなかったら
		if (!_isShot)
		{
			_bulletManager->Shot(_position);
			_isShot = true;
		}
	}

	//キーを押してない
	else
	{
		_isShot = false;
	}
}

//スコアアップ
void Player::ScoreUp()
{
	//加算
	_score += 10;

	//スコアラベル更新
	char str[16];
	wsprintf(str, "スコア：%d", _score);
	_scoreLabel->SetString(str);

	//30点取ったらクリアシーンへ
	if (_score >= 30)
	{
		Game::ChangeScene(new ClearScene);
	}
}