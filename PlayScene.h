#pragma once
#include "Scene.h"

//クラスのプロトタイプ宣言
class Player;
class Enemy;
class BulletManager;

//定数宣言
#define ENEMY_NUM	3		//敵の数(この値はヘッダーでも使うのでconstメンバ変数にはできない）)

/**************************************
　クリアシーンクラス
 ***************************************/
class PlayScene :	public Scene
{
	Player* _player;				//自機オブジェクト
	Enemy*	_enemy[ENEMY_NUM];		//敵オブジェクト
	BulletManager*	_bulletManager;	//弾の管理クラスオブジェクト

public:
	//コンストラクタ
	PlayScene();

	//デストラクタ
	~PlayScene();

	//初期化処理
	//引数：なし
	//戻値：なし
	void Start() override;

	//更新処理
	//引数：なし
	//戻値：なし
	void Update() override;
};

