#include "GameObject.h"

//コンストラクタ
GameObject::GameObject()
{
	//位置
	_position.x = 0.0f;
	_position.y = 0.0f;
	_position.z = 0.0f;

	//サイズ
	_size.x = 0.0f;
	_size.y = 0.0f;
	_size.z = 0.0f;

	//シーン
	_scene = NULL;
}

//デストラクタ
GameObject::~GameObject()
{
}

//更新処理
void GameObject::Update()
{
	//基本何もしない（大抵オーバーライドされる）
}

//描画処理
void GameObject::Draw()
{
	//基本何もしない（大抵オーバーライドされる）
}
