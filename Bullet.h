#pragma once
#include "Sprite.h"

//クラスのプロトタイプ宣言
class BulletManager;

/**************************************
　プレイヤーが撃った弾のクラス
 ***************************************/
class Bullet : public Sprite
{
	const int SPEED;			//移動速度
	BulletManager* _bulletManager;	//弾管理者

public:
	//コンストラクタ
	//引数：弾の管理者
	Bullet(BulletManager* bulletManager);

	//デストラクタ
	~Bullet();

	//更新処理
	//引数：なし
	//戻値：なし
	void Update() override;

};

