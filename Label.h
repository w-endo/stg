#pragma once
#include "GameObject.h"

/**************************************
　文字表示クラス
 ***************************************/
class Label :	public GameObject
{
	unsigned int _color;	//色
	char _string[128];		//表示内容

public:
	//コンストラクタ
	//引数：表示内容
	Label(char* string);

	//デストラクタ
	~Label();

	//描画処理
	//引数：なし
	//戻値：なし
	void Draw();


////////////////////////以下アクセス関数///////////////////////

	//文字色の設定
	//引数：r 赤（0〜255）
	//引数：g 緑（0〜255）
	//引数：b 青（0〜255）
	//戻値：なし
	void SetColor(int r, int g, int b)
	{
		_color = GetColor(r, g, b);
	}

	//表示する文字の変更
	//引数：表示したい文字
	//戻値：なし
	void SetString(char* string)
	{
		strcpy(_string, string);
	}
};

