#pragma once
#include <vector>

//クラスのプロトタイプ宣言
class GameObject;

/**************************************
　すべてのシーンの親クラス
 ***************************************/
class Scene
{
protected:
	//そのシーンに出てくるモノたち
	std::vector<GameObject*> _gameObject;
public:
	//コンストラクタ
	Scene();

	//デストラクタ
	virtual ~Scene();

	//初期化処理（純粋仮想関数）
	//引数：なし
	//戻値：なし
	virtual void Start() = 0;

	//更新処理
	//引数：なし
	//戻値：なし
	virtual void Update();

	//オブジェクトをシーンに追加
	//引数：object　追加するオブジェクト
	//戻値：なし
	void AddObject(GameObject* object);

	//指定したゲームオブジェクトをシーンから削除する
	//引数：object　削除するオブジェクト
	//戻値：なし
	void Destroy(GameObject* object);



};

