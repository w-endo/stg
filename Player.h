#pragma once
#include "Sprite.h"

//クラスのプロトタイプ宣言
class BulletManager;
class Label;

/**************************************
　自機クラス
 ***************************************/
class Player : public Sprite
{
	const int SPEED;	//移動速度

	int		_life;	//残機数
	int		_score;	//スコア
	bool	_isShot;//連射防止用ショットフラグ

	BulletManager*	_bulletManager;	//弾の管理者オブジェクト
	Label* _scoreLabel;				//スコア表示ラベルオブジェクト

	
public:
	//コンストラクタ
	Player();

	//デストラクタ
	~Player();

	//機能：更新処理
	//引数：なし
	//戻値：なし
	void Update();

	//機能：スコアアップ（敵を倒したときに呼ばれる）
	//引数：なし
	//戻値：なし
	void ScoreUp();


private:
	//機能：移動処理
	//引数：なし
	//戻値：なし
	void Move();

	//機能：射撃処理
	//引数：なし
	//戻値：なし
	void Shot();



///////////////////////以下アクセス関数/////////////////////
public:
	void SetBulletManager(BulletManager* bulletManager)
	{
		_bulletManager = bulletManager;
	}

	void SetScoreLabel(Label* scoreLabel)
	{
		_scoreLabel = scoreLabel;
	}
};

