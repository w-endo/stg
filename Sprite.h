#pragma once
#include "GameObject.h"

/**************************************
　画像を表示するためのクラス
 ***************************************/
class Sprite :public GameObject
{
protected:
	int _graphHandle;	//ロードした画像の番号
	int _srcX, _srcY, _width, _height;	//表示する範囲

public:
	//コンストラクタ
	Sprite();

	//デストラクタ
	~Sprite();

	//機能：読み込み処理
	//引数：fileName ファイル名
	//戻値：なし
	void Load(char* fileName);

	//機能：描画処理
	//引数：なし
	//戻値：なし
	void Draw();

	//表示範囲を指定
	//引数：x,y　範囲の左上座標
	//引数：w,h　範囲の幅と高さ
	//戻値：なし
	void SetRect(int x, int y, int w, int h);
};


