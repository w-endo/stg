#include "Enemy.h"

//コンストラクタ
Enemy::Enemy() : MAX_SPEED(5)
{
	//初期位置
	_position.x = GetRand(WINDOW_WIDTH);
	_position.y = GetRand(WINDOW_HEIGHT / 3);

	//移動速度
	_move.x = GetRand(MAX_SPEED * 2) - MAX_SPEED;
	_move.y = GetRand(MAX_SPEED);
	_move.z = 0;

	//画像読み込み
	Load("Assets\\enemy.png");
}

//デストラクタ
Enemy::~Enemy()
{
}

//更新処理
void Enemy::Update()
{
	//移動
	_position = VAdd(_position, _move);

	//画面外へ行った
	if (_position.x < -_size.x)			_position.x = WINDOW_WIDTH;	//左
	if (_position.x > WINDOW_WIDTH)		_position.x = -_size.x;		//右
	if (_position.y > WINDOW_HEIGHT)	_position.y = -_size.y;		//下
}
