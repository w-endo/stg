#include "Sprite.h"

//コンストラクタ
Sprite::Sprite()
{
	_graphHandle = 0;
	_srcX = _srcY = _width = _height = 0;
}

//デストラクタ
Sprite::~Sprite()
{
}

//読み込み処理
void Sprite::Load(char* fileName)
{
	//画像ファイルを読み込む
	_graphHandle = LoadGraph(fileName);

	//画像サイズを調べる
	int w, h;
	GetGraphSize(_graphHandle, &w, &h);
	_size.x = w;
	_size.y = h;

	//表示範囲を画像全体にしておく
	_width = w;
	_height = h;
}

//描画処理
void Sprite::Draw()
{
	DrawRectGraph(_position.x, _position.y, 
		_srcX, _srcY, _width, _height, _graphHandle, TRUE, FALSE);
}

//表示範囲を指定
void Sprite::SetRect(int x, int y, int w, int h)
{
	_srcX = x;
	_srcY = y;
	_width = w;
	_height = h;
}