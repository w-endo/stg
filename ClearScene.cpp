#include "ClearScene.h"
#include "Sprite.h"

//コンストラクタ
ClearScene::ClearScene()
{
}

//デストラクタ
ClearScene::~ClearScene()
{
}

//初期化処理
void ClearScene::Start()
{
	//クリア画像をシーンに追加
	Sprite* pict = new Sprite;
	pict->Load("Assets\\clear.bmp");
	AddObject(pict);
}
