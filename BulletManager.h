#pragma once
#include <vector>
#include "GameObject.h"

//クラスのプロトタイプ宣言
class Bullet;
class Enemy;

/**************************************
　プレイヤーが撃った弾を管理するクラス
***************************************/
class BulletManager :	public GameObject
{
	std::vector<Bullet*>	_bullet;	//複数の弾
public:
	//コンストラクタ
	BulletManager();

	//デストラクタ
	~BulletManager();

	//弾を撃つ
	//引数：発射位置（＝プレイヤーの位置）
	//戻値：なし
	void Shot(VECTOR position);

	//更新処理
	//引数：なし
	//戻値：なし
	void Update() override;

	//描画処理
	//引数：なし
	//戻値：なし
	void Draw() override;

	//弾と敵の当たり判定
	//引数：判定したい敵
	//戻値：当たった→true　当たってない→false
	bool CollisionEnemy(Enemy* enemy);

	//弾を削除
	//引数：bullet　削除する弾
	//戻値：なし
	void Destroy(Bullet* bullet);
};

