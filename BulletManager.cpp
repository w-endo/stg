#include "BulletManager.h"
#include "Bullet.h"
#include "Enemy.h"


//コンストラクタ
BulletManager::BulletManager()
{
}

//デストラクタ
BulletManager::~BulletManager()
{
}

//弾を撃つ
void BulletManager::Shot(VECTOR position)
{
	Bullet* bullet = new Bullet(this);
	bullet->SetPosition(position);
	_bullet.push_back(bullet);
}

//更新処理
void BulletManager::Update()
{
	//すべての弾を更新（移動）
	for (int i = 0; i < _bullet.size(); i++)
	{
		_bullet[i]->Update();
	}
}

//描画処理
void BulletManager::Draw()
{
	//すべての弾を描画
	for (int i = 0; i < _bullet.size(); i++)
	{
		_bullet[i]->Draw();
	}
}

//弾と敵の当たり判定
bool BulletManager::CollisionEnemy(Enemy* enemy)
{
	//すべての弾と判定
	for (int i = 0; i < _bullet.size(); i++)
	{
		//敵と弾の中心距離
		float dist = VSize(VSub(enemy->GetCenter(), _bullet[i]->GetCenter()));

		//当たった
		if (dist < 20)
		{
			//弾を削除
			Destroy(_bullet[i]);
			return true;
		}
	}

	//当たってない
	return false;
}


//弾を削除
void BulletManager::Destroy(Bullet* bullet)
{
	//削除するオブジェクトを配列から探す
	for (int i = 0; i < _bullet.size(); i++)
	{
		//見つけた
		if (_bullet[i] == bullet)
		{
			//メモリ開放
			delete _bullet[i];

			//配列から消去
			_bullet.erase(_bullet.begin() + i);

			break;
		}
	}
}