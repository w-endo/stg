#pragma once
#include "Global.h"

//クラスのプロトタイプ宣言
class Scene;

/**************************************
　ゲーム出てくるモノすべての親クラス
 ***************************************/
class GameObject
{
protected:
	VECTOR _position;	//位置
	VECTOR _size;		//サイズ
	Scene*	_scene;		//自分が追加されたシーン

public:
	//コンストラクタ
	GameObject();

	//デストラクタ
	virtual ~GameObject();

	//機能：更新処理
	//引数：なし
	//戻値：なし
	virtual void Update();

	//機能：描画処理
	//引数：なし
	//戻値：なし
	virtual void Draw();



/////////////////////////以下アクセス関数/////////////////////////

	//位置のセッター
	void SetPosition(VECTOR position)
	{
		_position = position;
	}

	//位置のセッター
	void SetPosition(float x, float y, float z=0)
	{
		_position.x = x;
		_position.y = y;
		_position.z = z;
	}


	//位置のゲッター
	VECTOR GetPosition()
	{
		return _position;
	}

	//中心位置を取得
	VECTOR GetCenter()
	{
		return VAdd(_position, VScale(_size, 0.5f));
	}

	//自分が追加されたシーンのセッター
	void SetScene(Scene* scene)
	{
		_scene = scene;
	}
};

