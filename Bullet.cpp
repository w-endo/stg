#include "Bullet.h"
#include "BulletManager.h"

//コンストラクタ
Bullet::Bullet(BulletManager* bulletManager) :SPEED(10)
{
	Load("Assets\\bullet.png");
	_bulletManager = bulletManager;
}

//デストラクタ
Bullet::~Bullet()
{
}

//更新処理
void Bullet::Update()
{
	//移動
	_position.y -= SPEED;

	//画面外へ出た
	if (_position.y < -_size.y)
	{
		//管理者に自分を削除してもらう
		_bulletManager->Destroy(this);
	}
}
