#include "Label.h"

//コンストラクタ
Label::Label(char* string)
{
	_color = GetColor(255, 255, 255);	//文字色（基本は白）
	SetString(string);	//表示内容をセット
}


Label::~Label()
{
}

void Label::Draw()
{
	DrawString(_position.x, _position.y, _string, _color);
}
