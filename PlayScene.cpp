#include "PlayScene.h"
#include "Player.h"
#include "Enemy.h"
#include "BulletManager.h"
#include "Label.h"
#include "Effect.h"

//コンストラクタ
PlayScene::PlayScene()
{
}

//デストラクタ
PlayScene::~PlayScene()
{
}

//初期化処理
void PlayScene::Start()
{
	//背景
	Sprite* background;
	background = new Sprite;
	background->Load("Assets\\back.bmp");
	AddObject(background);

	//プレイヤー
	_player = new Player;
	AddObject(_player);

	//敵
	for (int i = 0; i < ENEMY_NUM; i++)
	{
		_enemy[i] = new Enemy;
		AddObject(_enemy[i]);
	}

	//弾の管理者
	_bulletManager = new BulletManager;
	AddObject(_bulletManager);
	_player->SetBulletManager(_bulletManager);

	//スコア表示ラベル
	Label* score = new Label("スコア：0");
	score->SetPosition(10, 10);
	AddObject(score);
	_player->SetScoreLabel(score);
}


//更新処理
void PlayScene::Update()
{
	//シーン共通の更新処理
	Scene::Update();

	//プレイヤーがいれば
	if (_player)
	{
		//敵とプレイヤーの当たり判定
		for (int i = 0; i < ENEMY_NUM; i++)
		{
			//ｉ番目の敵が死んでたら無視
			if (!_enemy[i])	continue;

			//プレイヤーと敵の中心距離
			float dist = VSize(VSub(_player->GetCenter(), _enemy[i]->GetCenter()));

			//ぶつかってる
			if (dist < 50)
			{
				//プレイヤーを削除
				Destroy(_player);
				_player = NULL;
				break;
			}
		}
	}

	//敵と弾の当たり判定
	for (int i = 0; i < ENEMY_NUM; i++)
	{
		//ｉ番目の敵が死んでたら無視
		if (!_enemy[i])	continue;
		
		//ｉ番目の敵とすべての弾との当たり判定をする
		bool isHit = _bulletManager->CollisionEnemy(_enemy[i]);

		//当たってたら
		if (isHit)
		{
			//爆発エフェクトを表示
			Effect* bomb = new Effect;
			bomb->Load("assets\\bom.png");
			bomb->SetRect(0, 0, 64, 64);
			bomb->SetPosition(_enemy[i]->GetPosition());
			AddObject(bomb);

			//敵を消去
			Destroy(_enemy[i]);
			_enemy[i] = NULL;

			//点数アップ
			_player->ScoreUp();
			break;
		}
	}
}